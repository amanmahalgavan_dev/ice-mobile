"use strict";
//Authentication Routes
var index_1 = require('../auth/index');
//Common Routes
var user_routes_1 = require('../api/common/routes/user-routes');
var company_routes_1 = require('../api/common/routes/company-routes');
var utils_routes_1 = require('../api/common/routes/utils-routes');
//KO Routes
var internal_routes_1 = require('../api/popprobe/routes/internal-routes');
var data_routes_1 = require('../api/popprobe/routes/data-routes');
var Auth = require('../auth/auth-service');
var Routes = (function () {
    function Routes() {
    }
    Routes.init = function (app, router) {
        //Auth Routes
        index_1.AuthRoutes.init(router);
        //Protect the routes
        app.use(Auth.isAuthenticated());
        //Common Routes
        user_routes_1.UserRoutes.init(router);
        company_routes_1.CompanyRoutes.init(router);
        utils_routes_1.UtilRoutes.init(router);
        //KO Routes
        internal_routes_1.InternalRoutes.init(router);
        data_routes_1.DataRoutes.init(router);
        app.use('/', router);
    };
    return Routes;
}());
exports.Routes = Routes;
