"use strict";
require('../api/common/models/company-model');
var user_model_1 = require('../api/common/models/user-model');
var Auth = require('./auth-service');
var random = require('randomstring');
var mailer = require('../mailers/auth.mailer');
var AuthController = (function () {
    function AuthController() {
    }
    AuthController.login = function (req, res) {
        var data = req.body;
        //Verify data
        if (!data.email)
            return res.json({ error: true, message: "Email required." });
        if (!data.password)
            return res.json({ error: true, message: "Password required." });
        //Find user
        user_model_1["default"].findOne({ email: data.email.toLowerCase() }, function (err, user) {
            if (err)
                return res.json({ error: true, message: "An error Occured." });
            if (!user)
                return res.json({ error: true, message: "User not found." });
            if (!user.approved)
                return res.json({ error: true, message: "User is not approved." });
            //Verify password
            Auth.verify({ input: data.password, hashed: user.password }).then(function (status) {
                if (!status)
                    return res.json({ error: true, message: "Password is incorrect." });
                user.last_login = Date();
                user.save();
                //Create token
                var token = Auth.signToken(user._id);
                var company = user.company_id;
                user = user.toObject();
                delete user['company_id'];
                delete user['password'];
                res.cookie('token', JSON.stringify(token));
                //Return token and user
                return res.json({ error: false, message: 'Welcome, ' + user.name, data: { user: user, token: token, company: company } });
            }, function (err) {
                return res.json({ error: true, message: "An error Occured." });
            });
        }).populate({ path: 'company_id' });
    };
    AuthController.verify = function (req, res) {
        var data = req.body;
        var errors = validateVerifyData(data);
        if (Object.keys(errors).length != 0)
            return res.json({ error: true, message: 'Validation failed', errors: errors });
        var finder = {
            _id: data.user_id,
            reset_token: data.reset_token
        };
        user_model_1["default"].findOne(finder, function (err, user) {
            if (err)
                return res.json({ error: true, message: 'An error occured' });
            if (!user)
                return res.json({ error: true, message: 'Invalid token' });
            Auth.hash(data.password)
                .then(function (password) {
                var updater = {
                    $set: {
                        password: password,
                        verified: true,
                        updated_at: Date()
                    },
                    $unset: {
                        reset_token: 1
                    }
                };
                user_model_1["default"].update(finder, updater, function (err) {
                    if (err)
                        return res.json({ error: true, message: 'Password updated failed.' });
                    return res.json({ error: false, message: 'Password updated successfully.' });
                });
            }, function (error) {
                return res.json({ error: true, message: 'An error occured', data: error });
            });
        });
    };
    AuthController.reset = function (req, res) {
        var email = req.body.email;
        if (!email)
            return res.json({ error: true, message: 'Invalid Email' });
        user_model_1["default"].findOne({ email: email }, function (err, user) {
            if (err)
                return res.json({ error: true, message: 'An error occured' });
            if (!user)
                return res.json({ error: true, message: 'User not registered' });
            if (!user.approved)
                return res.json({ error: true, message: 'User is disabled' });
            var token = random.generate(20);
            var mail = new mailer(user.email);
            mail.reset(user.reset_token).then(function (body) {
                user.reset_token = token;
                user.save();
                return res.json({ error: false, message: "Please check mail to change password." });
            }, function (error) {
                return res.json({ error: true, message: error });
            });
        });
    };
    return AuthController;
}());
exports.AuthController = AuthController;
function validateVerifyData(data) {
    var errors = {};
    if (!data.reset_token)
        errors['reset_token'] = 'The token is required';
    if (!data.user_id)
        errors['user_id'] = 'The user id is required';
    if (!data.password)
        errors['password'] = 'The password is required';
    if (!data.password_confirmation)
        errors['password_confirmation'] = 'The password confirmation is required';
    if (data.password && data.password_confirmation && data.password != data.password_confirmation)
        errors['password'] = 'The password should be same as password confirmation';
    return errors;
}
