"use strict";
var bcrypt = require('bcrypt'), promise = require('bluebird'), expressJWT = require('express-jwt'), compose = require('composable-middleware'), jwt = require("jsonwebtoken"), config = require('../../constants/auth.json'), validateJwt = expressJWT({ secret: config.secret }).unless({ path: ['/api/auth/login', '/api/auth/register', '/api/auth/'] });
var user_model_1 = require('../api/common/models/user-model');
var mongoose = require('mongoose');
module.exports = {
    hash: function (password) {
        return new promise(function (resolve, reject) {
            var rounds = config.rounds || 10;
            bcrypt.hash(password, rounds, function (err, hash) {
                if (err)
                    return reject(err);
                resolve(hash);
            });
        });
    },
    verify: function (passwords) {
        return new promise(function (resolve, reject) {
            bcrypt.compare(passwords.input, passwords.hashed, function (err, status) {
                if (err)
                    return reject(err);
                //If status is false, return
                // if(! status) resolve(status);
                //Else create a JWT token and sent it back
                resolve(status);
            });
        });
    },
    signToken: function (id) {
        return jwt.sign({ _id: id }, config.secret, { expiresIn: 60 * 60 * 50 });
    },
    isAuthenticated: function () {
        // Validate jwt
        return compose().use(function (req, res, next) {
            // allow access_token to be passed through query parameter as well
            if (req.query && req.query.hasOwnProperty('access_token')) {
                req.headers.authorization = 'Bearer ' + req.query.access_token;
            }
            validateJwt(req, res, next);
        })
            .use(function (req, res, next) {
            // Attach user to request
            if (req.url.split('/')[2] === 'auth')
                return next();
            user_model_1["default"].findOne({ _id: req.user._id })
                .populate({ path: 'company_id' })
                .exec(function (err, user) {
                // console.log('BODY ',user);
                if (err)
                    return next(err);
                if (!user)
                    return res.status(401).json({ error: true, message: 'Unauthorized' });
                var company = user.company_id.toObject();
                if (!company.database || (company.database && !company.database.url))
                    return res.status(400).json({ error: true, message: 'Database not configured.' });
                // process.env.MONGO_URL = company.database.url;
                req.user = user;
                req.connection = mongoose.createConnection(company.database.url, function () {
                    next();
                });
            });
        });
    }
};
