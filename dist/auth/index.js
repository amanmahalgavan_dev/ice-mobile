"use strict";
var auth_controller_1 = require('./auth-controller');
var AuthRoutes = (function () {
    function AuthRoutes() {
    }
    AuthRoutes.init = function (router) {
        router
            .route('/api/auth/login')
            .post(auth_controller_1.AuthController.login);
        router
            .route('/api/auth/verify')
            .post(auth_controller_1.AuthController.verify);
        router
            .route('/api/auth/reset')
            .post(auth_controller_1.AuthController.reset);
    };
    return AuthRoutes;
}());
exports.AuthRoutes = AuthRoutes;
