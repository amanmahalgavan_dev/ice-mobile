"use strict";
var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var helmet = require('helmet');
var compression = require('compression');
var zlib = require('zlib');
var RoutesConfig = (function () {
    function RoutesConfig() {
    }
    RoutesConfig.init = function (application) {
        var _root = process.cwd();
        var _nodeModules = '/node_modules/';
        var _clientFiles = (process.env.NODE_ENV === 'production') ? '/client/dist/' : '/client/dev/';
        var allowCrossDomain = function (req, res, next) {
            if ('OPTIONS' == req.method) {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
                res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
                res.send(200);
            }
            else {
                next();
            }
        };
        application.use(allowCrossDomain);
        application.use(compression({
            level: zlib.Z_BEST_COMPRESSION,
            threshold: '1kb'
        }));
        application.use(express.static(_root + _nodeModules));
        application.use(express.static(_root + _clientFiles));
        application.use(bodyParser.json());
        application.use(bodyParser.urlencoded({ extended: true }));
        application.use(morgan('dev'));
        application.use(helmet());
        application.use("/static", express.static(process.cwd() + "/static"));
    };
    return RoutesConfig;
}());
exports.RoutesConfig = RoutesConfig;
