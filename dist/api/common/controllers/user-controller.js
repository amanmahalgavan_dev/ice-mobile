"use strict";
var user_model_1 = require('../models/user-model');
var random = require('randomstring');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var Auth = require('../../../auth/auth-service');
var mailer = require('../../../mailers/auth.mailer');
var _ = require('lodash');
var UserController = (function () {
    function UserController() {
    }
    UserController.index = function (req, res) {
        user_model_1["default"].find({}, { password: 0, reset_token: 0 }, function (err, users) {
            if (err)
                return res.json({ error: true, message: 'An error occured fetching users' });
            return res.json(users);
        }).populate({ path: "company_id" });
    };
    UserController.create = function (req, res) {
        var data = req.body;
        var errors = validateUserData(data);
        if (Object.keys(errors).length != 0)
            return res.json({ error: true, message: 'Validation failed', errors: errors });
        data.password = -1;
        data.verified = false;
        data.reset_token = random.generate(20);
        data.created_at = Date();
        data.company_id = mongoose.Types.ObjectId(data.company_id);
        console.log();
        user_model_1["default"].create(data, function (err, user) {
            if (err)
                return res.json({ error: true, message: 'An error occured', err: err });
            //Send an email to the user
            // let mail = new mailer(user.email);
            // mail.verify(user.reset_token).then(function(body: Object) {
            //   user = user.toObject();
            //   delete user.password;
            //   delete user.reset_token;
            //   return res.json({error: false, message: "User created successfully.", user, body}) 
            // }, function(error: any) {
            //   return res.json({error: true, message: error.response.body.errors[0].message, error});
            // });
            return res.json({ error: false, message: "User created successfully.", user: user });
        });
    };
    UserController.delete = function (req, res) {
        user_model_1["default"].findById(req.params.id, function (err, user) {
            if (err)
                return res.json({ error: true, message: 'An error occured', err: err });
            user.deleted = true;
            user.save(function (err) {
                if (err)
                    return res.json({ error: true, message: "Cannot delete user.", errorLog: err });
                return res.json({ error: false, message: "User deleted successfully." });
            });
        });
    };
    UserController.update = function (req, res) {
        user_model_1["default"].findById(req.params.id, function (err, user) {
            if (err)
                return res.json({ error: true, message: 'An error occured', err: err });
            var updated = _.merge(user, req.body);
            updated.save(function (err) {
                if (err)
                    return res.json({ error: true, message: "Cannot Update user.", errorLog: err });
                return res.json({ error: false, data: user, message: "User details updated successfully." });
            });
        });
    };
    return UserController;
}());
exports.UserController = UserController;
function validateUserData(data) {
    var errors = {};
    if (!data.name)
        errors['name'] = 'Name is required.';
    if (!data.email)
        errors['email'] = 'Email is required.';
    if (!data.allowed_modules || data.allowed_modules.length == 0)
        errors['modules'] = 'Modules are required.';
    if (!data.company_id)
        errors['company_id'] = 'Company name is required.';
    // if(data.email && !Validator.isEmail(data.email)) errors['email'] = "Email should be valid.";
    return errors;
}
