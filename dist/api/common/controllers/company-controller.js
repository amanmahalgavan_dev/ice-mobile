"use strict";
var company_model_1 = require('../models/company-model');
var _ = require('lodash');
var CompanyController = (function () {
    function CompanyController() {
    }
    CompanyController.index = function (req, res) {
        company_model_1["default"].find({}, function (err, companies) {
            if (err)
                return res.json({ error: true, message: 'An error occured fetching companies' });
            return res.json(companies);
        });
    };
    CompanyController.create = function (req, res) {
        var data = req.body;
        var errors = validateCompanyData(data);
        if (Object.keys(errors).length != 0)
            return res.json({ error: true, message: 'Validation failed', errors: errors });
        data.created_at = Date();
        company_model_1["default"].create(data, function (err, company) {
            if (err)
                return res.json({ error: true, message: 'An error occured', err: err });
            return res.json({ error: false, message: "Company added successfully.", company: company });
        });
    };
    CompanyController.delete = function (req, res) {
        company_model_1["default"].findById(req.params.id, function (err, company) {
            if (err)
                return res.json({ error: true, message: 'An error occured', err: err });
            company.deleted = true;
            company.save(function (err) {
                if (err) {
                    return handleError(res, err);
                }
                return res.status(204).send('No Content');
            });
        });
    };
    CompanyController.update = function (req, res) {
        company_model_1["default"].findById(req.params.id, function (err, company) {
            if (err)
                return res.json({ error: true, message: 'An error occured', err: err });
            // if(!lookup) { return res.status(404).send('Not Found'); }
            var updated = _.merge(company, req.body);
            updated.save(function (err) {
                if (err) {
                    return handleError(res, err);
                }
                return res.status(200).json(company);
            });
        });
    };
    return CompanyController;
}());
exports.CompanyController = CompanyController;
function validateCompanyData(data) {
    var errors = {};
    if (!data.name)
        errors['name'] = 'Name is required.';
    if (!data.logo)
        errors['logo'] = 'Logo is required.';
    if (!data.modules || !data.modules.length)
        errors['Modules'] = 'Modules are required.';
    return errors;
}
function handleError(res, err) {
    return res.status(500).send(err);
}
