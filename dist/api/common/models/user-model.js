"use strict";
var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    company_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Company', index: true },
    name: String,
    email: { type: String, required: true, unique: true, index: true },
    password: { type: String, required: true },
    avatar: String,
    address: String,
    telephone: String,
    cellphone: String,
    fax: String,
    company: String,
    profile: String,
    role: { type: String, default: false },
    access: Array,
    approved: { type: Boolean, default: 1 },
    allowed_modules: { type: Array, required: true },
    email_frequency: String,
    verified: Boolean,
    reset_token: String,
    last_login: Date,
    updated_at: Date,
    created_at: Date,
    deleted: { type: Boolean, default: false }
});
function findNotDeletedMiddleware(next) {
    this.where({ deleted: { $ne: true } });
    next();
}
schema.pre('find', findNotDeletedMiddleware);
schema.pre('findOne', findNotDeletedMiddleware);
exports.__esModule = true;
exports["default"] = mongoose.model('User', schema);
