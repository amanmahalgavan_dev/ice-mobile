"use strict";
var utils_controller_1 = require('../controllers/utils-controller');
var multer = require('multer');
// const upload = multer({dest:'/uploads'})
var upload = multer({ dest: (process.cwd() + '/uploads') });
var UtilRoutes = (function () {
    function UtilRoutes() {
    }
    UtilRoutes.init = function (router) {
        router
            .route('/api/utils/upload')
            .post(upload.single('file'), utils_controller_1.UtilsController.upload);
    };
    return UtilRoutes;
}());
exports.UtilRoutes = UtilRoutes;
