"use strict";
var user_controller_1 = require('../controllers/user-controller');
var UserRoutes = (function () {
    function UserRoutes() {
    }
    UserRoutes.init = function (router) {
        router
            .route('/api/users')
            .get(user_controller_1.UserController.index)
            .post(user_controller_1.UserController.create);
        router
            .route('/api/users/:id')
            .put(user_controller_1.UserController.update)
            .delete(user_controller_1.UserController.delete);
    };
    return UserRoutes;
}());
exports.UserRoutes = UserRoutes;
