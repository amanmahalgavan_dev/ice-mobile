"use strict";
var company_controller_1 = require('../controllers/company-controller');
var CompanyRoutes = (function () {
    function CompanyRoutes() {
    }
    CompanyRoutes.init = function (router) {
        router
            .route('/api/companies')
            .get(company_controller_1.CompanyController.index)
            .post(company_controller_1.CompanyController.create);
        router
            .route('/api/companies/:id')
            .put(company_controller_1.CompanyController.update)
            .delete(company_controller_1.CompanyController.delete);
    };
    return CompanyRoutes;
}());
exports.CompanyRoutes = CompanyRoutes;
