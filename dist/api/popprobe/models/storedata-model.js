"use strict";
var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    cust_sk: { type: Number, required: true, index: true },
    cust_name: { type: String, required: true, index: true },
    date_id: { type: Number, required: true, index: true },
    country_id: { type: Number, required: true, index: true },
    city_id: { type: Number, required: true, index: true },
    channel_id: { type: Number, required: true, index: true },
    sub_channel_id: { type: Number, required: true, index: true },
    cooler: { type: Boolean, required: true },
    railing: { type: Boolean, required: true },
    price_surveyed: { type: Boolean, required: true },
    fresh_surveyed: { type: Boolean, required: true },
    score: { type: Number, required: true },
    data: Object,
    images: Array
}, { collection: 'store_data' });
function StoreData(connection) {
    return connection.model('StoreData', schema);
}
exports.__esModule = true;
exports["default"] = StoreData;
