"use strict";
var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    poc_sovi_number_sk: String,
    poc_sovi_sk: String,
    poc_sovi_desc: String,
    cool_device: String,
    poc_sovi_number: String,
    barcode: String,
    poc_description: String,
    poc_owner: String
});
function Sovi(connection) {
    return connection.model('Sovi', schema);
}
exports.__esModule = true;
exports["default"] = Sovi;
