"use strict";
var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    id: { type: Number, required: true, unique: true, index: true },
    title: { type: String, required: true, index: true },
    channel_id: { type: Number, index: true },
    mpa_title: String
}, { collection: 'sub_channels' });
function SubChannel(connection) {
    return connection.model('SubChannel', schema);
}
exports.__esModule = true;
exports["default"] = SubChannel;
