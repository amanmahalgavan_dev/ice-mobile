"use strict";
var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    country_id: { type: Number, index: true },
    id: { type: Number, unique: true, index: true },
    title: { type: String, required: true, index: true },
    original: { type: Array },
    lat: String,
    lng: String
});
function City(connection) {
    return connection.model('City', schema);
}
exports.__esModule = true;
exports["default"] = City;
