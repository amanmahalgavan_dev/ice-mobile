"use strict";
var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    cust_sk: { type: Number, unique: true, index: true },
    country_id: { type: Number, index: true },
    city_id: { type: Number, index: true },
    channel_id: { type: Number, index: true },
    sub_channel_id: { type: Number, index: true },
    lat: String,
    lng: String,
    address: String,
    telephone: String
});
function Customer(connection) {
    return connection.model('Customer', schema);
}
exports.__esModule = true;
exports["default"] = Customer;
