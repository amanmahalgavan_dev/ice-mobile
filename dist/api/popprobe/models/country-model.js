"use strict";
var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    id: { type: Number, unique: true, index: true },
    title: { type: String, index: true, unique: true },
    iso: { type: String },
    lat: String,
    lng: String
});
function Country(connection) {
    return connection.model('Country', schema);
}
exports.__esModule = true;
exports["default"] = Country;
