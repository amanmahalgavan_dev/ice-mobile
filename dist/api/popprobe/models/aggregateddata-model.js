"use strict";
var mongoose = require('mongoose');
var collection = 'aggregated_data';
var schema = new mongoose.Schema({
    date_id: { type: Number, required: true, index: true },
    country_id: { type: Number, index: true, null: true, sparse: true },
    city_id: { type: Number, index: true, null: true, sparse: true },
    channel_id: { type: Number, index: true, null: true, sparse: true },
    sub_channel_id: { type: Number, index: true, null: true, sparse: true },
    cooler: { type: Boolean, index: true, null: true, sparse: true },
    store_count: { type: Number, required: true },
    data: Object
}, { collection: collection });
function AggregatedData(connection) {
    return connection.model('AggregatedData', schema, collection);
}
exports.__esModule = true;
exports["default"] = AggregatedData;
