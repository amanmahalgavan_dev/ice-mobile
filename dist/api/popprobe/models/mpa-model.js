"use strict";
var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    //For all countries except TT
    country: { type: String, required: true, index: true },
    description: { type: String, required: true },
    brand_package_code: { type: Number, required: true },
    category: { type: String, required: true },
    ignore: { type: Boolean, default: false },
    //For TT we have additional key (as different su channels have different mpa)
    moms_pops: { type: Boolean, default: false },
    mini_markets: { type: Boolean, default: false },
    cafeterias: { type: Boolean, default: false },
    category_cafeterias: { type: String }
}, { collection: 'mpa' });
function MPA(connection) {
    return connection.model('MPA', schema);
}
exports.__esModule = true;
exports["default"] = MPA;
