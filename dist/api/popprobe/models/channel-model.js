"use strict";
var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    id: { type: Number, required: true, unique: true },
    title: { type: String, required: true, unique: true, index: true }
});
function Channel(connection) {
    return connection.model('Channel', schema);
}
exports.__esModule = true;
exports["default"] = Channel;
