"use strict";
var mongoose = require('mongoose');
var schema = new mongoose.Schema({
    sk: { type: Number, unique: true, index: true },
    code: { type: String, required: true },
    name: { type: String, required: true }
});
function Auditor(connection) {
    return connection.model('Auditor', schema);
}
exports.__esModule = true;
exports["default"] = Auditor;
