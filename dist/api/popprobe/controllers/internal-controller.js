"use strict";
var auditor_model_1 = require('../models/auditor-model');
var channel_model_1 = require('../models/channel-model');
var subchannel_model_1 = require('../models/subchannel-model');
var country_model_1 = require('../models/country-model');
var city_model_1 = require('../models/city-model');
var date_model_1 = require('../models/date-model');
var customer_model_1 = require('../models/customer-model');
var storedata_model_1 = require('../models/storedata-model');
var sovi_model_1 = require('../models/sovi-model');
var fs = require('fs');
var InternalController = (function () {
    function InternalController() {
    }
    InternalController.insertData = function (req, res) {
        var root = process.cwd();
        var dirname = root.replace('ice2', 'toJSON\\');
        var filenames = ['auditors.json', 'channels.json', 'sub_channels.json', 'countries.json', 'cities.json', 'dates.json', 'customers.json', 'images.json', 'sovis.json'];
        // var filenames = ['images.json'];
        filenames.forEach(function (filename) {
            fs.readFile(dirname + filename, 'utf-8', function (err, content) {
                if (err)
                    return res.json({ err: err });
                content = JSON.parse(content);
                switch (filename) {
                    case 'auditors.json':
                        auditors(content);
                        break;
                    case "channels.json":
                        channels(content);
                        break;
                    case 'sub_channels.json':
                        sub_channels(content);
                        break;
                    case 'countries.json':
                        countries(content);
                        break;
                    case 'cities.json':
                        cities(content);
                        break;
                    case 'dates.json':
                        dates(content);
                        break;
                    case 'customers.json':
                        customers(content);
                        break;
                    case 'sovis.json':
                        sovis(content);
                        break;
                    case 'Store':
                        stores(content);
                    default: break;
                }
            });
        });
        return res.json({ messages: "Files are being read", filenames: filenames });
    };
    return InternalController;
}());
exports.InternalController = InternalController;
function auditors(data) {
    console.log("The data at auditors is " + data.length);
    data.forEach(function (entry) {
        auditor_model_1["default"].create(entry);
    });
}
function channels(data) {
    console.log("The data at channels is " + data.length);
    data.forEach(function (entry) {
        channel_model_1["default"].create(entry);
    });
}
function sub_channels(data) {
    console.log("The data at sub channels is " + data.length);
    data.forEach(function (entry) {
        subchannel_model_1["default"].create(entry);
    });
}
function countries(data) {
    console.log("The data at countries is " + data.length);
    data.forEach(function (entry) {
        country_model_1["default"].create(entry);
    });
}
function cities(data) {
    console.log("The data at cities is " + data.length);
    data.forEach(function (entry) {
        city_model_1["default"].create(entry);
    });
}
function dates(data) {
    console.log("The data at dates is " + data.length);
    data.forEach(function (entry) {
        date_model_1["default"].create(entry);
    });
}
function customers(data) {
    console.log("The data at customers is " + data.length);
    data.forEach(function (entry) {
        customer_model_1["default"].create(entry);
    });
}
function stores(data) {
    console.log("The data at stores is " + data.length);
    data.forEach(function (image) {
        storedata_model_1["default"].create(image);
    });
}
function sovis(data) {
    console.log("The data at channels is " + data.length);
    data.forEach(function (entry) {
        sovi_model_1["default"].create(entry);
    });
}
