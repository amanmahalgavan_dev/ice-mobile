"use strict";
var aggregateddata_model_1 = require('../models/aggregateddata-model');
var channel_model_1 = require('../models/channel-model');
var city_model_1 = require('../models/city-model');
var country_model_1 = require('../models/country-model');
var subchannel_model_1 = require('../models/subchannel-model');
var date_model_1 = require('../models/date-model');
var storedata_model_1 = require('../models/storedata-model');
var aggregator_1 = require('../queries/aggregator');
var async = require('async');
var DataController = (function () {
    function DataController() {
    }
    /**
    * Returns the data for the dashboard
    */
    DataController.dashboard = function (req, res) {
        var aggregate = [], input = req.query;
        if (!input.date_id)
            return res.json({ error: true, message: "The date is required." });
        aggregate = aggregator_1.Aggregator.dashboard(input);
        aggregateddata_model_1["default"](req.connection).aggregate(aggregate, function (err, data) {
            if (err)
                return res.json({ error: true, message: 'An error occured', err: err });
            return res.json({ error: false, message: null, data: data });
        });
    };
    /**
    * Return the data for the stores
    */
    DataController.store = function (req, res) {
        var aggregate = [], input = req.query;
        if (!input.date_id)
            return res.json({ error: true, message: "The date is required." });
        aggregate = aggregator_1.Aggregator.store(input);
        storedata_model_1["default"](req.connection).aggregate(aggregate).allowDiskUse(true).exec(function (err, data) {
            if (err)
                return res.json({ error: true, message: 'An error occured', err: err });
            return res.json({ error: false, message: null, data: data });
        });
    };
    /*
        Searches stores for of almost matching names with data for currently surveyed month
    */
    DataController.search = function (req, res) {
        var aggregate = [], input = new RegExp(req.query.q);
        aggregate = aggregator_1.Aggregator.search(input);
        storedata_model_1["default"](req.connection).aggregate(aggregate).allowDiskUse(true).exec(function (err, data) {
            if (err)
                return res.json({ error: true, message: 'An error occured', err: err });
            return res.json({ error: false, message: null, data: data });
        });
    };
    DataController.filters = function (req, res) {
        //Get all the channels 
        function getChannels(callback) {
            channel_model_1["default"](req.connection).find({}, function (err, channel) {
                callback(err, channel);
            });
        }
        function getCities(channel, callback) {
            city_model_1["default"](req.connection).find({}, function (err, cities) {
                callback(err, channel, cities);
            });
        }
        function getCoolers(channel, city, callback) {
            var Coolers = [
                { title: "Yes", id: 1 },
                { title: "No", id: 0 }
            ];
            callback(null, channel, city, Coolers);
        }
        function getCountries(channel, cities, coolers, callback) {
            country_model_1["default"](req.connection).find({}, function (err, countries) {
                callback(err, channel, cities, coolers, countries);
            });
        }
        function getDates(channel, cities, coolers, countries, callback) {
            date_model_1["default"](req.connection).find({}, function (err, dates) {
                callback(err, channel, cities, coolers, countries, dates);
            });
        }
        function getsubchannel(channel, cities, coolers, countries, dates, callback) {
            subchannel_model_1["default"](req.connection).find({}, function (err, subchannels) {
                var filters = {
                    channels: channel,
                    cities: cities,
                    countries: countries,
                    coolers: coolers,
                    dates: dates,
                    sub_channels: subchannels
                };
                callback(err, filters);
            });
        }
        async.waterfall([
            getChannels,
            getCities,
            getCoolers,
            getCountries,
            getDates,
            getsubchannel,
        ], function (err, result) {
            if (err)
                return res.json({ error: true, message: 'An error occured' });
            return res.status(201).json({ data: result, status_code: 200, success: true });
        });
    };
    return DataController;
}());
exports.DataController = DataController;
