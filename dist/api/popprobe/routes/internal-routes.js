"use strict";
var internal_controller_1 = require('../controllers/internal-controller');
var InternalRoutes = (function () {
    function InternalRoutes() {
    }
    InternalRoutes.init = function (router) {
        router
            .route('/api/internals')
            .post(internal_controller_1.InternalController.insertData);
    };
    return InternalRoutes;
}());
exports.InternalRoutes = InternalRoutes;
