"use strict";
var data_controller_1 = require('../controllers/data-controller');
var export_controller_1 = require('../controllers/export-controller');
var DataRoutes = (function () {
    function DataRoutes() {
    }
    DataRoutes.init = function (router) {
        router.route('/api/dashboard').get(data_controller_1.DataController.dashboard);
        router.route('/api/stores').get(data_controller_1.DataController.store);
        router.route('/api/filters').get(data_controller_1.DataController.filters);
        router.route('/api/stores/export').get(export_controller_1.ExportController.export);
        router.route('/api/stores/search').get(data_controller_1.DataController.search);
    };
    return DataRoutes;
}());
exports.DataRoutes = DataRoutes;
