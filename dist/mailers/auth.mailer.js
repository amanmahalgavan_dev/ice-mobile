"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var mailer_1 = require('./mailer');
var AuthMailer = (function (_super) {
    __extends(AuthMailer, _super);
    function AuthMailer(toEmail) {
        _super.call(this, toEmail);
    }
    AuthMailer.prototype.reset = function (token) {
        _super.prototype.subject = "Reset Password | PopProbe.com";
        _super.prototype.html = "Hello " + token;
        return _super.prototype.send.call(this);
    };
    AuthMailer.prototype.verify = function (token) {
        _super.prototype.subject = "Verify Email | PopProbe.com";
        _super.prototype.html = "Hello " + token;
        return _super.prototype.send.call(this);
    };
    return AuthMailer;
}(mailer_1.Mailer));
module.exports = AuthMailer;
