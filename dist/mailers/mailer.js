"use strict";
var helper = require('sendgrid').mail;
var promise = require('bluebird');
var sg = require('sendgrid')(process.env.SENDGRID_API_KEY);
var Mailer = (function () {
    function Mailer(email) {
        this.toEmail = email;
        this.fromEmail = process.env.FROM_EMAIL || 'no-reply@popprobe.com';
    }
    Mailer.prototype.send = function () {
        var mail = new helper.Mail(this.fromEmail, this.subject, this.toEmail, this.html);
        var request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON()
        });
        return new promise(function (resolve, reject) {
            sg.API(request, function (err, response) {
                if (err)
                    return reject(err);
                resolve(response.body);
            });
        });
    };
    return Mailer;
}());
exports.Mailer = Mailer;
