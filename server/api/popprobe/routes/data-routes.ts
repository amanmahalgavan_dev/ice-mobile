"use strict";

import * as express from 'express';
import {DataController} from '../controllers/data-controller';
import {ExportController} from '../controllers/export-controller';

export class DataRoutes {
    static init(router: express.Router) {
      router.route('/api/dashboard').get(DataController.dashboard);
      router.route('/api/stores').get(DataController.store);
      router.route('/api/filters').get(DataController.filters);
      router.route('/api/stores/export').get(ExportController.export);
      router.route('/api/stores/search').get(DataController.search);
    }
}
