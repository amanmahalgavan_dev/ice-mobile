"use strict";

import * as express from 'express';
import {InternalController} from '../controllers/internal-controller';

export class InternalRoutes {
    static init(router: express.Router) {
      router
        .route('/api/internals')
        .post(InternalController.insertData);
    }
}
