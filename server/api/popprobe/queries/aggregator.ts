import {Utils} from '../services/utils';
const _ = require('lodash'); 
export class Aggregator {
	/*
	returns eggregation pipeline to search store matching regular expressions
	*/
	static search (input: any) {
		return [
			{
				$match:{
				    cust_name: {
				    	$regex: input, 
				    	$options: 'i'
				    }
				}
			},
			{$sort: {cust_sk:1, date_id:-1}},
		    {
		        $lookup: {
		            from: "dates",
		            localField: "date_id",
		            foreignField: "id",
		            as: "date"
		        }
		    },
		    {
		        $unwind: "$date"
		    },
		    {$project: {
		        _id: 0, cooler: 1, store_count: 1,cust_sk:1, images: 1, cust_name: 1,score: 1, data: 1,
		            date: { id: 1, year: 1, month: 1, date: 1 }
		        }
		    }
		];
	}

	/** 
	* Will return the aggregate pipeline for the query for getting stores
	*/
	static store (input: any) {
	    return [
	        {$match: Utils.getFinderWithOutNull(input)},
	        {$sort: {date_id:-1}},
	        {
	            $group: {
	                _id: "$cust_sk", 
	                data: {
	                    $push :{
	                        cust_sk : "$cust_sk",
	                        cust_name : "$cust_name",
	                        date_id : "$date_id" ,
	                        country_id : "$country_id",
	                        city_id : "$city_id",
	                        channel_id : "$channel_id",
	                        sub_channel_id : "$sub_channel_id" ,
	                        cooler : "$cooler",
	                        railing : "$railing" ,
	                        price_surveyed : "$price_surveyed" ,
	                        fresh_surveyed : "$fresh_surveyed" ,
	                        score : "$score",
	                        images: "$images",
	                        data: "$data"
	                    }
	                }
	            }
	        },
	        {
	            $project: {
	                images: "$images",
	                current: {
	                    $arrayElemAt: ["$data", 0]    
	                },
	                previous: {
	                    $arrayElemAt: ["$data", 1]    
	                }
	            }
	        },
	        {
	            $project:{
	                _id: 0,
	                cust_sk: "$_id",
	                cust_name : "$current.cust_name",
	                date_id : "$current.date_id",
	                country_id : "$current.country_id",
	                city_id : "$current.city_id",
	                channel_id : "$current.channel_id",
	                sub_channel_id : "$current.sub_channel_id",
	                images: "$images",
	                data: {
	                   current: {
	                        data : "$current.data",
	                        date_id: "$current.date_id",
	                        cooler : "$current.cooler",
	                        railing : "$current.railing",
	                        price_surveyed : "$current.price_surveyed",
	                        fresh_surveyed : "$current.fresh_surveyed",
	                        score : "$current.score",
	                   },
	                   previous: {
	                        data : "$previous.data",
	                        date_id: "$previous.date_id",
	                        cooler : "$previous.cooler",
	                        railing : "$previous.railing",
	                        price_surveyed : "$previous.price_surveyed",
	                        fresh_surveyed : "$previous.fresh_surveyed",
	                        score : "$previous.score",
	                   }
	                }
	            }
	        },
	        {
	            $match: {date_id:Number(input.date_id)}
	        },
	        {
	            $lookup: {
	                from: "dates",
	                localField: "date_id",
	                foreignField: "id",
	                as: "date"
	            }
	        },
	        {
	            $lookup: {
	                from: "countries",
	                localField: "country_id",
	                foreignField: "id",
	                as: "country"
	            }
	        },
	        {
	            $lookup: {
	                from: "cities",
	                localField: "city_id",
	                foreignField: "id",
	                as: "city"
	            }
	        },
	        {
	            $lookup: {
	                from: "channels",
	                localField: "channel_id",
	                foreignField: "id",
	                as: "channel"
	            }
	        },
	        {
	            $lookup: {
	                from: "sub_channels",
	                localField: "sub_channel_id",
	                foreignField: "id",
	                as: "sub_channel"
	            }
	        },
	        {
	            $unwind: "$date"
	        },
	        {
	            $unwind: "$country"
	        },
	        {
	            $unwind: "$city"
	        },
	        {
	            $unwind: "$channel"
	        },
	        {
	            $unwind: "$sub_channel"
	        },
	        {
	            $project: { 
	                _id: 0, cooler: 1, store_count: 1,cust_sk:1, images: 1, cust_name: 1,score: 1, data: 1,
	                date: { id: 1, year: 1, month: 1, date: 1 },
	                country: { id: 1, title: 1, iso: 1, lat: 1, lng: 1 },
	                city: { id: 1, title: 1, lat: 1, lng: 1, iso: 1, country_id: 1 },
	                channel: { id: 1, title: 1 },
	                sub_channel: { id: 1, title: 1, channel_id: 1 }
	            }
	        }
	    ];
	}

	/** 
	* Will return the aggregate pipeline for the query for getting dashboard data
	*/
	static dashboard (input: any) {
		var match = [{$match: Utils.getFinderWithNull(input)}], 
        lookup = [], 
        unwind = [], 
        project = [{
            $project: 
            { 
                _id: 0, cooler: 1, store_count: 1, score: 1, data: 1,
                date: { id: 1, year: 1, month: 1, date: 1 },
                country: { id: 1, title: 1, iso: 1, lat: 1, lng: 1 },
                city: { id: 1, title: 1, lat: 1, lng: 1, iso: 1, country_id: 1 },
                channel: { id: 1, title: 1 },
                sub_channel: { id: 1, title: 1, channel_id: 1 }
            }
        }],
        keyIndex, 
        inputKeys = Object.keys(input);

	    // lookup and unwind the collection only for those keys exist in input
	    inputKeys.forEach(item => {
	        lookup.push({
	            $lookup: {
	                from: Utils.getCollectionName(item, false),
	                localField: item,
	                foreignField: "id",
	                as: Utils.getCollectionName(item, true)
	            }
	        });
	        unwind.push({$unwind: "$" + Utils.getCollectionName(item, true)});
	    });

	    // merging all the array to create aggregation pipeline
	    return _.union(match, lookup, unwind, project); 
	}
} 