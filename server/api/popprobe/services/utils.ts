export class Utils {
	/**
	* Will create a filter object to find the data based on the input
	*/
	static getFinderWithNull(input) {
	    let finder  = {
	        date_id: parseInputKey(input.date_id, 'number')
	    };
	    //Check country
	    (input.country_id)        ? finder['country_id'] = parseInputKey(input.country_id, 'number')         : finder['country_id'] = null;
	    //Check city
	    (input.city_id)           ? finder['city_id'] = parseInputKey(input.city_id, 'number')               : finder['city_id'] = null;
	    //Check channel
	    (input.channel_id)        ? finder['channel_id'] = parseInputKey(input.channel_id, 'number')         : finder['channel_id'] = null;
	    //Check sub channel
	    (input.sub_channel_id)    ? finder['sub_channel_id'] = parseInputKey(input.sub_channel_id, 'number') : finder['sub_channel_id'] = null;
	    //Check cooler
	    (input.cooler)            ? finder['cooler'] = Number(input.cooler)                                  : finder['cooler'] = null;
	    return finder;
	}

	/**
	* Will create a filter object to find the data based on the input
	*/
	static getFinderWithOutNull(input) {
	    let finder  = {
	        'date_id': parseInputKey(input.date_id, 'number')
	    };
	    //Check customer
	    if(input.cust_sk)        finder['cust_sk'] = parseInputKey(input.cust_sk, 'number');
	    //Check country
	    if(input.country_id)     finder['country_id'] = parseInputKey(input.country_id, 'number');
	    //Check city
	    if(input.city_id)        finder['city_id'] = parseInputKey(input.city_id, 'number');
	    //Check channel
	    if(input.channel_id)     finder['channel_id'] = parseInputKey(input.channel_id, 'number');
	    //Check sub channel
	    if(input.sub_channel_id) finder['sub_channel_id'] = parseInputKey(input.sub_channel_id, 'number');
	    //Check cooler
	    if(input.cooler)         finder['cooler'] = Number(input.cooler);
	    //Check railing
	    if(input.railing)         finder['railing'] = Number(input.railing);
	    return finder;
	}


	/*
	returns collections name / projector name
	*/
	static getCollectionName (key, c) {
	    if(key === 'date_id') {
	        if(c) return 'date';
	        return 'dates';
	    } 
	    if(key === 'country_id') {
	        if (c) return 'country';
	        return 'countries';
	    }
	    if(key === 'city_id') {
	        if (c) return 'city';
	        return 'cities';
	    }
	    if(key === 'channel_id') {
	        if (c) return 'channel';
	        return 'channels';
	    }
	    if(key === 'sub_channel_id') {
	        if (c) return 'sub_channel';
	        return 'sub_channels';
	    }
	    if(key === 'cooler') {
	        if (c) return 'cooler';
	        return 'coolers';
	    }
	}
}

/**
* Will split the input by pipe (1|2|3) => ['1', '2', '3']
* Optional flag is type. If tpye is number, it will cast the array items to Number ['1', '2', '3'] => [1, 2, 3]
* Return {$in: [1, 2, 3]}
*/
function parseInputKey(string, type) {
    let val = string.split('|');

    if(type == 'number') {
        let data = [];
        val.forEach(item => {
            data.push(Number(item));
        });
        return {$in: data};
    }
    return {$in: val};
}
