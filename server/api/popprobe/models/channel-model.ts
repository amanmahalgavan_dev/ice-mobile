import * as mongoose from 'mongoose';

var schema = new mongoose.Schema({
    id: {type: Number, required: true, unique: true},
    title: {type: String, required: true, unique: true, index: true},
});
export default function Channel (connection) {
	return connection.model('Channel', schema);
} 