import * as mongoose from 'mongoose';

var schema = new mongoose.Schema({
    sk: {type: Number, unique: true, index: true},
    code: {type: String, required: true},
    name: {type: String, required: true}
});
export default function Auditor (connection) {
	return connection.model('Auditor', schema);
} 