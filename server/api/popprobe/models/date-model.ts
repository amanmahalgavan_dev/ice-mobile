import * as mongoose from 'mongoose';

var schema = new mongoose.Schema({
    id: {type:Number, unique: true, required: true, index: true},
    year: {type: Number, required: true},
    month: {type: Number, required: true},
    date: {type: Number, unique: true, index: true},
    current: {type: Boolean, default: false}
});
export default function Date (connection) {
	return connection.model('Date', schema);
}
