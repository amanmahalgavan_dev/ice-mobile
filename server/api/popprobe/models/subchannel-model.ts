import * as mongoose from 'mongoose';

var schema = new mongoose.Schema({
    id: {type: Number, required: true, unique: true, index: true},
    title: {type: String, required: true, index: true},
    channel_id: {type:Number, index: true},
    mpa_title: String
}, {collection: 'sub_channels'});

export default function SubChannel (connection) {
	return connection.model('SubChannel', schema);
} 