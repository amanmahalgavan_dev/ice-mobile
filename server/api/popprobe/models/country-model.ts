import * as mongoose from 'mongoose';

var schema = new mongoose.Schema({
    id: {type:Number, unique: true, index: true},
    title: {type: String, index: true, unique: true},
    iso: {type: String},
    lat: String,
    lng: String,
});
export default function Country (connection) {
	return connection.model('Country', schema);
}  