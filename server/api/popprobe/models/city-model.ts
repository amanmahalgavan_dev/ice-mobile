import * as mongoose from 'mongoose';

var schema = new mongoose.Schema({
    country_id: {type:Number, index: true},
    id: {type: Number, unique: true, index: true},
    title: {type: String, required: true, index: true},
    original: {type: Array},
    lat: String,
    lng: String
});
export default function City (connection) {
	return connection.model('City', schema);
} 