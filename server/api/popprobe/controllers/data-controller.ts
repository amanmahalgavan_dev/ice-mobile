import * as express from 'express';
import AggregatedData from '../models/aggregateddata-model';
import Channel from '../models/channel-model';
import City from '../models/city-model';
import Country from '../models/country-model';
import Subchannel from '../models/subchannel-model';
import Date from '../models/date-model';
import StoreData from '../models/storedata-model';
import {Aggregator} from '../queries/aggregator';
const async = require('async');

export class DataController {
    /**
    * Returns the data for the dashboard 
    */  
    static dashboard(req: express.Request, res: express.Response) {
        let aggregate = [], input   = req.query;
        if( ! input.date_id) return res.json({error: true, message: "The date is required."});
            aggregate = Aggregator.dashboard(input);
        AggregatedData(req.connection).aggregate(aggregate, function (err: any, data: any) {
            if(err) return res.json({error: true, message: 'An error occured', err: err});
                return res.json({error: false, message: null, data});
        });
    }
     
    /**
    * Return the data for the stores
    */
    static store(req: express.Request, res: express.Response) {
        let aggregate = [], input = req.query;
        if(!input.date_id) return res.json({error: true, message: "The date is required."});
            aggregate = Aggregator.store(input);
        StoreData(req.connection).aggregate(aggregate).allowDiskUse(true).exec(function (err: any, data: any) {
            if(err) return res.json({error: true, message: 'An error occured', err: err});
                return res.json({error: false, message: null, data});
        });       
    }

    /*
        Searches stores for of almost matching names with data for currently surveyed month
    */
    static search (req: express.Request, res: express.Response) {
        let aggregate = [], input = new RegExp(req.query.q);
        aggregate = Aggregator.search(input);
        StoreData(req.connection).aggregate(aggregate).allowDiskUse(true).exec(function (err: any, data: any) {
            if(err) return res.json({error: true, message: 'An error occured', err: err});
                return res.json({error: false, message: null, data});
        });
    }
    
    static filters (req: express.Request, res: express.Response) {
        //Get all the channels 
        function getChannels(callback){
            Channel(req.connection).find({},function(err, channel){
                callback(err, channel)
            });
        }

        function getCities(channel, callback){
            City(req.connection).find({},function(err, cities){
                callback(err, channel, cities)
            });
        }

        function getCoolers(channel,city, callback){
            var Coolers = [
                {title:"Yes", id: 1},
                {title:"No", id: 0}
            ];
            callback(null, channel, city, Coolers);
        }

        function getCountries(channel,cities, coolers, callback){
            Country(req.connection).find({},function(err, countries){
               callback(err, channel,cities, coolers, countries)
            });
        }

        function getDates(channel,cities, coolers, countries, callback){
            Date(req.connection).find({},function(err, dates){
               callback(err, channel,cities, coolers, countries,dates)
            });
        }

        function getsubchannel(channel,cities, coolers, countries,dates, callback){
            Subchannel(req.connection).find({},function(err, subchannels){
                var filters = {
                    channels : channel,
                    cities : cities,
                    countries : countries,
                    coolers : coolers,
                    dates : dates,
                    sub_channels : subchannels
                }
               callback(err, filters)
            });
        }

        async.waterfall([
          getChannels,
          getCities,
          getCoolers,
          getCountries,
          getDates,
          getsubchannel,
        ],
        function(err,result){
          if(err) return res.json({error: true, message: 'An error occured'});
          return res.status(201).json({data : result,status_code: 200, success : true});
        });
    }
}