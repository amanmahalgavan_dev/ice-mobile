import * as express from 'express';

import StoreData from '../models/storedata-model';
const _ = require('lodash'); 
const Excel = require('exceljs');
// var options = {
   
//     useStyles: true,
//     useSharedStrings: true
// };
// const workbook = new Excel.stream.xlsx.WorkbookWriter(options);
const filename = (process.cwd()+'/uploads/' +'streamed-workbook.xlsx');
const workbook = new Excel.Workbook();
workbook.creator = 'Abdul';
workbook.views = [
  {
    x: 0, y: 0, width: 10000, height: 20000, 
    firstSheet: 0, activeTab: 1, visibility: 'visible'
  }
]
var sheet = workbook.addWorksheet('My Sheet');

const worksheet = workbook.getWorksheet('My Sheet');

worksheet.columns = [
    { header: 'SK', key: 'id', width: 10 },
    { header: 'Name', width: 30 },
    { header: 'Country' ,  width: 20  },
    { header: 'City' ,  width: 20  },
    { header: 'Channel' ,  width: 30  },
    { header: 'Sub Channel' ,  width: 30  },
    { header: 'Address' ,  width: 20  },
    { header: 'Telephone' ,  width: 20  },
    { header: 'Date' ,  width: 20  },
    { header: 'Cooler' ,  width: 20  },
    { header: 'Railing' ,  width: 20  },
    { header: 'Total' ,  width: 20  },
    { header: 'Mpa' ,  width: 20  },
    { header: 'Sovi' ,  width: 20  },
    { header: 'Refrigeration' ,  width: 20  },
    { header: 'Communication exhibition' ,  width: 30  },
    { header: 'Price compliance' ,  width: 20  },
    { header: 'Product freshness' ,  width: 20  },
    // { header: 'Additional Exhibition' },
    { header: 'Combo' ,  width: 20 },
    { header: 'Communication' ,  width: 20 },
    { header: 'Cold availability' ,  width: 20  },
];
// var sheet = workbook.addWorksheet('My Sheet');
var arr = [],rowArr = [];
export class ExportController {

  static export(req: express.Request, res: express.Response) {
    let aggregate = [], input   = req.query;
    if( ! input.date_id) return res.json({error: true, message: "The date is required."});
    aggregate = getStoreAggregator(input, 'store');
      StoreData.aggregate(aggregate, function(err, storedata) {
        if(err) return res.json({error: true, message: 'An error occured'});
        console.log(storedata.length);
        var coolerCondition, railingCondition;
        for (var i = 0; i < storedata.length; i++) {
          if(storedata[i].cooler == 1 ||storedata[i].railing == 1  ){
            coolerCondition = "Yes";
            railingCondition = "Yes";
          }else{
            coolerCondition ="No";
            railingCondition ="No";
          }
          arr.push(storedata[i].cust_sk);
          arr.push(storedata[i].cust_name);
          arr.push(storedata[i].country.title);
          arr.push(storedata[i].city.title);
          arr.push(storedata[i].channel.title);
          arr.push(storedata[i].sub_channel.title);
          arr.push(storedata[i].address);
          arr.push(storedata[i].telephone);
          arr.push(storedata[i].date.date);
          arr.push(coolerCondition);
          arr.push(railingCondition);
          arr.push(storedata[i].data[0].score);
          arr.push(storedata[i].data[0]['childern'][0].score);
          arr.push(storedata[i].data[0]['childern'][1].score);
          arr.push(storedata[i].data[0]['childern'][2].score);
          if(storedata[i].channel.id == 1){
            arr.push(storedata[i].data[0]['childern'][3].score);
            arr.push(storedata[i].data[0]['childern'][4].score);
            arr.push(storedata[i].data[0]['childern'][5].score);
          }          
          else{
            arr.push('NA');
            arr.push('NA');
            arr.push('NA');
          }
          if(storedata[i].channel.id == 3){
            arr.push(storedata[i].data[0]['childern'][3].score);
            arr.push(storedata[i].data[0]['childern'][4].score);
            arr.push(storedata[i].data[0]['childern'][5].score);
          }
          else{
            arr.push('NA');
            arr.push('NA');
            arr.push('NA');
          }
          rowArr.push(arr);
          arr = [];
        }
        worksheet.addRows(rowArr);
      //   res.setHeader("Content-Type", "application/vnd.ms-excel;");
      // res.setHeader("Content-disposition", `attachment;filename=report.xls`);
      //  // worksheet.commit();
      //  workbook.xlsx.write(res)
      //   .then(function(){
      //   res.end();
      //   });
        // // console.log(rowArr)
        workbook.xlsx.writeFile(filename)
          .then(function() {
              // done
              console.log(filename);
              res.download(filename, function(err,res){
                  console.log('---------- error downloading file: ' + err + res);
              });

          });
        // res.setHeader("Content-Type", "application/vnd.ms-excel;")
        // res.setHeader("Content-disposition", `attachment;filename=report.xls`)
        // // write to a stream
        // // workbook.xlsx.write(stream)
        // //   .then(function() {
        // //       // done
        // //       console.log(stream);
        // //   });
        // return res.json({error: false, message: null, storedata});
      });

      // cursor.on("data", (data) => {
      //   worksheet.addRow(data)
      // });
      // res.setHeader("Content-Type", "application/vnd.ms-excel;")
      // res.setHeader("Content-disposition", `attachment;filename=report.xls`)

      // cursor.once("end", () => {
      //   workbook.commit()
      //   workbook.xlsx.write(res)
      //   res.end()
      // })
  }
}

/**
 * Will create a filter object to find the data based on the input
 */
function getFinderWithNull(input) {
    let finder  = {
        date_id: parseInputKey(input.date_id, 'number')
    };
    //Check country
    (input.country_id)        ? finder['country_id'] = parseInputKey(input.country_id, 'number')         : finder['country_id'] = null;
    //Check city
    (input.city_id)           ? finder['city_id'] = parseInputKey(input.city_id, 'number')               : finder['city_id'] = null;
    //Check channel
    (input.channel_id)        ? finder['channel_id'] = parseInputKey(input.channel_id, 'number')         : finder['channel_id'] = null;
    //Check sub channel
    (input.sub_channel_id)    ? finder['sub_channel_id'] = parseInputKey(input.sub_channel_id, 'number') : finder['sub_channel_id'] = null;
    //Check cooler
    (input.cooler)            ? finder['cooler'] = Number(input.cooler)                                  : finder['cooler'] = null;
    return finder;
}

/**
 * Will create a filter object to find the data based on the input
 */
function getFinderWithOutNull(input) {
    let finder  = {
        date_id: parseInputKey(input.date_id, 'number')
    };
    //Check country
    if(input.country_id)     finder['country_id'] = parseInputKey(input.country_id, 'number');
    //Check city
    if(input.city_id)        finder['city_id'] = parseInputKey(input.city_id, 'number');
    //Check channel
    if(input.channel_id)     finder['channel_id'] = parseInputKey(input.channel_id, 'number');
    //Check sub channel
    if(input.sub_channel_id) finder['sub_channel_id'] = parseInputKey(input.sub_channel_id, 'number');
    //Check cooler
    if(input.cooler)         finder['cooler'] = Number(input.cooler);
    return finder;
}


/**
 * Will split the input by pipe (1|2|3) => ['1', '2', '3']
 * Optional flag is type. If tpye is number, it will cast the array items to Number ['1', '2', '3'] => [1, 2, 3]
 * Return {$in: [1, 2, 3]}
 */
function parseInputKey(string, type) {
    let val = string.split('|');

    if(type == 'number') {
        let data = [];
        val.forEach(item => {
            data.push(Number(item));
        });
        return {$in: data};
    }

    return {$in: val};
}

/** 
 * Will return the aggregate pipeline for the query
 */
// function getAggregator(input: any, type: String) {
//     var finder, 
//         projector = {
//             _id: 0, cooler: 1, store_count: 1, score: 1, data: 1,
//             date: {
//                 id: 1, year: 1, month: 1, date: 1
//             }
//         };

//     if(type == 'store') {
//         projector['cust_name'] = 1;
//         projector['cust_sk'] = 1;
//         finder = getFinderWithOutNull(input);
//     } else {
//         finder = getFinderWithNull(input);
//     }
//     let match = [{
//             $match: finder
//         }],
//         lookUp = [{
//             $lookup:
//             {
//                 from: "dates",
//                 localField: "date_id",
//                 foreignField: "id",
//                 as: "date"
//             }
//         }], 
//         unWind = [{$unwind: "$date"}],
//         project = {
//             $project: projector
//         };

//     if(input.country_id) {
//         lookUp.push({
//             $lookup:
//             {
//                 from: "countries",
//                 localField: "country_id",
//                 foreignField: "id",
//                 as: "country"
//             }
//         });
//         unWind.push({$unwind: "$country"});
//         project.$project['country'] = {id: 1, title: 1, lat: 1, lng: 1, iso: 1} 
//     }
//     if(input.city_id) {
//         lookUp.push({
//             $lookup:
//             {
//                 from: "cities",
//                 localField: "city_id",
//                 foreignField: "id",
//                 as: "city"
//             }
//         });
//         unWind.push({$unwind: "$city"});
//         project.$project['city'] = {id: 1, title: 1, lat: 1, lng: 1, iso: 1, country_id: 1} 
//     }
//     if(input.channel_id) {
//         lookUp.push({
//             $lookup:
//             {
//                 from: "channels",
//                 localField: "channel_id",
//                 foreignField: "id",
//                 as: "channel"
//             }
//         });
//         unWind.push({$unwind: "$channel"});
//         project.$project['channel'] = {id: 1, title: 1} 
//     }
//     if(input.sub_channel_id) {
//         lookUp.push({
//             $lookup:
//             {
//                 from: "sub_channels",
//                 localField: "sub_channel_id",
//                 foreignField: "id",
//                 as: "sub_channel"
//             }
//         });
//         unWind.push({$unwind: "$sub_channel"});
//         project.$project['sub_channel'] = {id: 1, title: 1, channel_id: 1} 
//     }
//     //Union arrays
//     let result = _.union(match, lookUp, unWind);
//     //Push project
//     result.push(project);
//     return result;

function getStoreAggregator(input: any) {
   return [
       {
           $match: getFinderWithOutNull(input)
       },
       {
           $lookup: {
               from: "dates",
               localField: "date_id",
               foreignField: "id",
               as: "date"
           }
       }, 
       {
           $lookup: {
               from: "countries",
               localField: "country_id",
               foreignField: "id",
               as: "country"
           }
       }, 
       {
           $lookup: {
               from: "cities",
               localField: "city_id",
               foreignField: "id",
               as: "city"
           }
       }, 
       {
           $lookup: {
               from: "channels",
               localField: "channel_id",
               foreignField: "id",
               as: "channel"
           }
       }, 
       {
           $lookup: {
               from: "sub_channels",
               localField: "sub_channel_id",
               foreignField: "id",
               as: "sub_channel"
           }
       },
       {$unwind: "$date"},
       {$unwind: "$country"},
       {$unwind: "$city"},
       {$unwind: "$channel"},
       {$unwind: "$sub_channel"},
       {
           $project: {
               _id: 0, cust_sk:1, cust_name: 1, score: 1, date_id:1, cooler: 1, railing:1, images:1, data: 1,
               date: { id: 1, year: 1, month: 1, date: 1 },
               country: {id: 1, title: 1, lat: 1, lng: 1, iso: 1},
               city: {id: 1, title: 1, lat: 1, lng: 1, iso: 1, country_id: 1},
               channel: {id: 1, title: 1},
               sub_channel: {id: 1, title: 1, channel_id: 1}
           }
       }
   ];
}