import * as express from 'express';

import Auditor from '../models/auditor-model';
import Channel from '../models/channel-model';
import SubChannel from '../models/subchannel-model';
import Country from '../models/country-model';
import City from '../models/city-model';
import Date from '../models/date-model';
import Customer from '../models/customer-model';
import StoreData from '../models/storedata-model';
import Sovi from '../models/sovi-model';


const fs = require('fs');

export class InternalController {

  static insertData(req: express.Request, res: express.Response) {

        var root = process.cwd();

        var dirname = root.replace('ice2', 'toJSON\\');
        var filenames = ['auditors.json', 'channels.json','sub_channels.json', 'countries.json', 'cities.json', 'dates.json', 'customers.json', 'images.json', 'sovis.json'];
        
        // var filenames = ['images.json'];

        filenames.forEach(function(filename) {
            fs.readFile(dirname + filename, 'utf-8', function(err, content) {
                if (err) return res.json({err});
                content = JSON.parse(content);
                switch(filename) {
                    case 'auditors.json':
                        auditors(content);
                        break;
                    case "channels.json":
                        channels(content);
                        break;
                    case 'sub_channels.json':
                        sub_channels(content);
                        break;
                    case 'countries.json':
                        countries(content);
                        break;
                    case 'cities.json':
                        cities(content);
                        break;
                    case 'dates.json':
                        dates(content);
                        break;
                    case 'customers.json':
                        customers(content);                        
                        break;
                    case 'sovis.json':
                        sovis(content);                        
                        break;
                    case 'Store':
                        stores(content);
                    default: break;
                }
            });
        });
        return res.json({messages: "Files are being read", filenames});
  }
}

function auditors(data) {
    console.log("The data at auditors is " + data.length);    
    data.forEach(entry => {
        Auditor.create(entry);
    });
}

function channels(data) {
    console.log("The data at channels is " + data.length);
    data.forEach(entry => {
        Channel.create(entry);
    });
}

function sub_channels(data) {
    console.log("The data at sub channels is " + data.length);    
    data.forEach(entry => {
        SubChannel.create(entry);
    });
}

function countries(data) {
    console.log("The data at countries is " + data.length);    
    data.forEach(entry => {
        Country.create(entry);
    });
}

function cities(data) {
    console.log("The data at cities is " + data.length);    
    data.forEach(entry => {
        City.create(entry);
    });
}

function dates(data) {
    console.log("The data at dates is " + data.length);    
    data.forEach(entry => {
        Date.create(entry);
    });
}

function customers(data) {
    console.log("The data at customers is " + data.length);    
    data.forEach(entry => {
        Customer.create(entry);
    });
}

function stores(data) {
    console.log("The data at stores is " + data.length);    
    data.forEach(image => {
        StoreData.create(image);
    });
    
}

function sovis(data) {
    console.log("The data at channels is " + data.length);    
    data.forEach(entry => {
        Sovi.create(entry);
    });
}
