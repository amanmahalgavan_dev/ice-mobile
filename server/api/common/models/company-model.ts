import * as mongoose from 'mongoose';

var schema = new mongoose.Schema({
    name: {type: String, required: true, unique: true, index: true},
    logo: {type: String, required: true},
    modules: {type: Array, required: true},
    deleted : {type : Boolean, default : false},
    updated_at: Date,
    created_at: Date
});

function findNotDeletedMiddleware(next) {
    this.where({ deleted : { $ne : true } });
    next();
}

schema.pre('find', findNotDeletedMiddleware);
schema.pre('findOne', findNotDeletedMiddleware);

export default mongoose.model('Company', schema);

