"use strict";

import * as express from 'express';
import {CompanyController} from '../controllers/company-controller';

export class CompanyRoutes {
    static init(router: express.Router) {
      router
        .route('/api/companies')
        .get(CompanyController.index)
        .post(CompanyController.create);
      router
        .route('/api/companies/:id')
        .put(CompanyController.update)
        .delete(CompanyController.delete);
      
    }
}
