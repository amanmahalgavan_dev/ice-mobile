"use strict";

import * as express from 'express';
import {UserController} from '../controllers/user-controller';

export class UserRoutes {
    static init(router: express.Router) {
      router
        .route('/api/users')
        .get(UserController.index)
        .post(UserController.create);
       router
        .route('/api/users/:id')
        .put(UserController.update)
        .delete(UserController.delete);
    }
}
