"use strict";

import * as express from 'express';
import {UtilsController} from '../controllers/utils-controller';
const multer = require('multer')
// const upload = multer({dest:'/uploads'})
var upload = multer({dest: (process.cwd() +'/uploads')});
export class UtilRoutes {
    static init(router: express.Router) {
      router
        .route('/api/utils/upload')
        .post(upload.single('file'), UtilsController.upload)
    }
}
