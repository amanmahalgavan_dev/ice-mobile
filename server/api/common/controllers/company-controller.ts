import * as express from 'express';

import Company from '../models/company-model';

const _ = require('lodash');


export class CompanyController {
  static index(req: express.Request, res: express.Response) {
    Company.find({}, function(err, companies) {
      if(err) return res.json({error: true, message: 'An error occured fetching companies'});
      return res.json(companies);
    });
  }

  static create(req: express.Request, res: express.Response) {
    var data = req.body;
    var errors = validateCompanyData(data);
    if(Object.keys(errors).length != 0) return res.json({error: true, message: 'Validation failed', errors}); 

    data.created_at = Date();

    Company.create(data, function (err, company) {
      if(err) return res.json({error: true, message: 'An error occured', err});
      return res.json({error: false, message: "Company added successfully.", company});
    });
  }

  static delete(req: express.Request, res: express.Response) {
    Company.findById(req.params.id, function (err, company) {
      if(err) return res.json({error: true, message: 'An error occured', err});
      company.deleted = true;
      company.save(function(err) {
        if(err) { return handleError(res, err); }
        return res.status(204).send('No Content');
      });
    });
  }

  static update(req: express.Request, res: express.Response) {
    Company.findById(req.params.id, function (err, company) {
      if(err) return res.json({error: true, message: 'An error occured', err});
      // if(!lookup) { return res.status(404).send('Not Found'); }
      var updated = _.merge(company, req.body);
      updated.save(function (err) {
        if (err) { return handleError(res, err); }
        return res.status(200).json(company);
      });
    });
  }
}

function validateCompanyData (data: any) {
  var errors = {};
  if (!data.name) errors['name'] = 'Name is required.';
  if (!data.logo) errors['logo'] = 'Logo is required.';
  if (!data.modules || !data.modules.length) errors['Modules'] = 'Modules are required.';
  return errors;
}

function handleError(res, err) {
  return res.status(500).send(err);
}

