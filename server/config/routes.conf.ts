"use strict";

import * as express from 'express';
import * as morgan from 'morgan';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import * as compression from 'compression';
import * as zlib from 'zlib';

export class RoutesConfig {
    static init(application: express.Application):void {
        let _root = process.cwd();
        let _nodeModules = '/node_modules/';
        let _clientFiles = (process.env.NODE_ENV === 'production') ? '/client/dist/' : '/client/dev/';
        let allowCrossDomain = function (req: express.Request, res: express.Response, next: express.NextFunction) {
            if ('OPTIONS' == req.method) {
                res.header('Access-Control-Allow-Origin', '*');
                res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
                res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
                res.send(200);
            }
            else {
                next();
            }
        };
        application.use(allowCrossDomain);
        application.use(compression({
            level: zlib.Z_BEST_COMPRESSION,
            threshold: '1kb'
        }));
        application.use(express.static(_root + _nodeModules));
        application.use(express.static(_root + _clientFiles));
        application.use(bodyParser.json());
        application.use(bodyParser.urlencoded({ extended: true }));
        application.use(morgan('dev'));
        application.use(helmet());
        application.use("/static", express.static(process.cwd() + "/static"));
    }
}