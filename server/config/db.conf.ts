"use strict";

import * as mongoose from 'mongoose';
var dbConst = require('../../constants/db.json');

export class DBConfig {
    static init(url:any) {
      const URL = url || dbConst.localhost;
      mongoose.connect(URL);
      mongoose.connection.on('error', console.error.bind(console, 'An error ocurred with the DB connection: '));
    }
};