var helper  = require('sendgrid').mail;
var promise = require('bluebird');
var sg      = require('sendgrid')(process.env.SENDGRID_API_KEY);

export class Mailer {
    
    public toEmail: string;
    public fromEmail: string;
    public subject: string;
    public html: string;

    public constructor(email: string) { 
        this.toEmail = email; 
        this.fromEmail = process.env.FROM_EMAIL || 'no-reply@popprobe.com';
    }


    send() {
        var mail = new helper.Mail(this.fromEmail, this.subject, this.toEmail, this.html);

        var request = sg.emptyRequest({
            method: 'POST',
            path: '/v3/mail/send',
            body: mail.toJSON(),
        });

        return new promise(function(resolve: any, reject: any) {
            sg.API(request, function(err: any, response: any) {
                if (err) return reject(err);
                resolve(response.body);
            });
	    });
    }
}