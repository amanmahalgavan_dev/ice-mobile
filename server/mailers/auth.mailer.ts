import {Mailer} from './mailer';

class AuthMailer extends Mailer {

    constructor(toEmail: string) {
        super(toEmail);
    }

    reset(token: String) {
        super.subject    = "Reset Password | PopProbe.com";
        super.html       = "Hello " + token;
        return super.send();
    }

    verify(token: String) {
        super.subject    = "Verify Email | PopProbe.com";
        super.html       = "Hello " + token;
        return super.send();
    }
}

module.exports = AuthMailer;