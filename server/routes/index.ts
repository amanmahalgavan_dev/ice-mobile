import * as express from 'express';

//Authentication Routes
import {AuthRoutes} from '../auth/index';
//Common Routes
import {UserRoutes} from '../api/common/routes/user-routes';
import {CompanyRoutes} from '../api/common/routes/company-routes';
import {UtilRoutes} from '../api/common/routes/utils-routes';

//KO Routes
import {InternalRoutes} from '../api/popprobe/routes/internal-routes';
import {DataRoutes} from '../api/popprobe/routes/data-routes';

const Auth = require('../auth/auth-service');

export class Routes {
    static init(app: express.Application, router: express.Router) {
        //Auth Routes
        AuthRoutes.init(router);
        
        //Protect the routes
        app.use(Auth.isAuthenticated());

        //Common Routes
        UserRoutes.init(router);
        CompanyRoutes.init(router);
        UtilRoutes.init(router);

        //KO Routes
        InternalRoutes.init(router);
        DataRoutes.init(router);

        app.use('/', router);
    }
}