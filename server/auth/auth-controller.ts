import * as express from 'express';

require('../api/common/models/company-model');
import User from '../api/common/models/user-model';
const Auth = require('./auth-service');
const random   = require('randomstring');
const mailer   = require('../mailers/auth.mailer');

export class AuthController {
  static login(req: express.Request, res: express.Response) {
      var data = req.body;
      //Verify data
      if(!data.email) return res.json({error: true, message: "Email required."});
      if(!data.password) return res.json({error: true, message: "Password required."});
      //Find user
      User.findOne({email: data.email.toLowerCase()}, function (err: any, user: any) {
          if(err) return res.json({error: true, message: "An error Occured."}); 
          if(!user) return res.json({error: true, message: "User not found."}); 
          if(!user.approved) return res.json({error: true, message: "User is not approved."}); 
          //Verify password
          Auth.verify({input:data.password, hashed:user.password}).then(function (status) {
            if(!status) return res.json({error: true, message: "Password is incorrect."});
            user.last_login = Date();
            user.save();
            //Create token
            var token = Auth.signToken(user._id);
            var company = user.company_id;

            user = user.toObject();

            delete user['company_id'];
            delete user['password'];

            res.cookie('token', JSON.stringify(token));
            //Return token and user
            return res.json({error: false, message:'Welcome, ' + user.name, data:{user, token, company}});
          }, function (err) {
            return res.json({error: true, message: "An error Occured."});
          });
      }).populate({path: 'company_id'});
  }

  static verify(req: express.Request, res: express.Response) {
    var data = req.body;
    var errors = validateVerifyData(data);
    if(Object.keys(errors).length != 0) return res.json({error: true, message: 'Validation failed', errors});

    var finder = {
      _id: data.user_id,
      reset_token: data.reset_token
    };

    User.findOne(finder, function(err, user) {
      if(err) return res.json({error: true, message: 'An error occured'});
      if(! user) return res.json({error: true, message: 'Invalid token'});

      Auth.hash(data.password)
        .then(function(password) {  

          let updater = {
            $set: {
              password: password,
              verified: true,
              updated_at: Date()
            },
            $unset: {
              reset_token: 1
            }
          }

          User.update(finder, updater, function(err) {
            if(err) return res.json({error: true, message: 'Password updated failed.'});
            return res.json({error: false, message: 'Password updated successfully.'});
          });
        }, function(error) {
          return res.json({error: true, message: 'An error occured', data: error});
        })
    })
  }

  static reset(req: express.Request, res: express.Response) { 
    var email = req.body.email;
    if(!email) return res.json({error: true, message: 'Invalid Email'});
    User.findOne({email: email}, function(err: any, user: any) {
      if(err) return res.json({error: true, message: 'An error occured'});
      if(! user) return res.json({error: true, message: 'User not registered'});
      if(!user.approved) return res.json({error: true, message: 'User is disabled'});
 
      var token = random.generate(20);
      let mail = new mailer(user.email);
      mail.reset(user.reset_token).then(function(body: Object) {
        user.reset_token = token;
        user.save();
        return res.json({error: false,message: "Please check mail to change password."});
      }, function(error: any) {
        return res.json({error:true, message: error});
      });
    });
  }
}

function validateVerifyData(data) {
  var errors = {};
  if(! data.reset_token)   errors['reset_token'] = 'The token is required';
  if(! data.user_id)       errors['user_id'] = 'The user id is required';
  if(! data.password)      errors['password'] = 'The password is required';
  if(! data.password_confirmation) errors['password_confirmation'] = 'The password confirmation is required';
  if(data.password && data.password_confirmation && data.password != data.password_confirmation) 
    errors['password'] = 'The password should be same as password confirmation';
  return errors;
}