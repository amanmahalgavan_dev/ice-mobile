"use strict";

import * as express from 'express';
import {AuthController} from './auth-controller';

export class AuthRoutes {
    static init(router: express.Router) {
  	router
      .route('/api/auth/login')
      .post(AuthController.login);

    router
      .route('/api/auth/verify')
      .post(AuthController.verify);

    router
      .route('/api/auth/reset')
      .post(AuthController.reset);
    }
}
