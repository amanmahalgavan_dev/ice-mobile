var bcrypt = require('bcrypt'),
	promise = require('bluebird'),
	expressJWT = require('express-jwt'),
	compose = require('composable-middleware'),
	jwt = require("jsonwebtoken"),
	config = require('../../constants/auth.json'),
	validateJwt = expressJWT({ secret: config.secret }).unless({path: ['/api/auth/login', '/api/auth/register', '/api/auth/']});
import User from '../api/common/models/user-model';
import * as express from 'express';
import {DBConfig} from '../config/db.conf';
import * as mongoose from 'mongoose';

module.exports = {
	hash: function(password: String) {
		return new promise(function(resolve: any, reject: any) {
			var rounds = config.rounds || 10;
			bcrypt.hash(password, rounds, function (err: any, hash: String) {
	            if (err) return reject(err);
	            resolve(hash);
			})
	    });
	},
	verify: function(passwords: any) {
		return new promise(function(resolve: any, reject: any) {
			bcrypt.compare(passwords.input, passwords.hashed, function (err: any, status: Boolean) {
	            if (err) return reject(err);
	            //If status is false, return
	            // if(! status) resolve(status);
	            //Else create a JWT token and sent it back
	            resolve(status);
			});
	    });
	}, 
	
	signToken: function (id: String) {
		return jwt.sign({ _id: id }, config.secret, { expiresIn: 60 * 60 * 50 }); 
	},

	isAuthenticated: function() {
		// Validate jwt
		return compose().use(function(req, res, next) {
			// allow access_token to be passed through query parameter as well
			if(req.query && req.query.hasOwnProperty('access_token')) {
				req.headers.authorization = 'Bearer ' + req.query.access_token;
			}
			validateJwt(req, res, next);
		})
		.use(function(req, res, next) {
			// Attach user to request
			if(req.url.split('/')[2] === 'auth')
				return next();
			User.findOne({_id: req.user._id})
			.populate({path: 'company_id'})
			.exec(function (err:any, user:any) {
				// console.log('BODY ',user);
				if (err) return next(err);
				if (!user) return res.status(401).json({error: true, message: 'Unauthorized'});
				let company = user.company_id.toObject();
				if(! company.database || (company.database && ! company.database.url))
					return res.status(400).json({error: true, message: 'Database not configured.'});
				// process.env.MONGO_URL = company.database.url;
				req.user = user;
				req.connection = mongoose.createConnection(company.database.url, function () {
					next();
				});
			});
		})	
	}
}
